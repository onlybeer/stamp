app.factory('authorize', function($q, $state, $rootScope, Usuario, $window, $timeout, DB) {
  return function(url) {
    var deferred = $q.defer();
    if(localStorage.getItem('usuarioId')){
      Usuario.get({
        id: localStorage.getItem('usuarioId')
      }).then(function(res){
        if(res){
          $rootScope.usuario = {
            ID: res.ID,
            NOME: res.NOME_FANTASIA,
            EMAIL: res.EMAIL,
            UF: res.UF,
            REGIAO: res.REGIAO,
          };
          deferred.resolve();
        } else {
          $timeout(function(){
            $state.go('login');
          });
        }
      }).catch(function(){
        console.log('erro desconhecido');
      });
    } else {
      $timeout(function(){
        $state.go('login');
      });
      deferred.reject();
    }
    return deferred.promise;
  };
});




app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider.state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppController'
  })
   // -- LOGIN
   .state('login', {
    cache: false,
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginController'
  })

   .state('app.catalogo', {
    url: '/catalogo',
    views: {
      'menuContent': {
        templateUrl: 'templates/catalogo.html',
        controller: 'CatalogoCtrl'
      }
    },
    resolve: {
      authorize: ['authorize', function(authorize) {
        return authorize(this.name);
      }]
    }
  })

   .state('app.clientes', {
    url: '/clientes',
    views: {
      'menuContent': {
        templateUrl: 'templates/clientes.html',
        controller: 'ClientesCtrl'
      }
    },
    resolve: {
      authorize: ['authorize', function(authorize) {
        return authorize(this.name);
      }]
    }
  })

    .state('app.cliente', {
    url: '/cliente/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/cliente.html',
        controller: 'ClienteCtrl'
      }
    },
    resolve: {
      authorize: ['authorize', function(authorize) {
        return authorize(this.name);
      }]
    }
  })

   .state('app.produto', {
    url: '/produto/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/produto.html',
        controller: 'ProdutoCtrl'
      }
    },
    resolve: {
      authorize: ['authorize', function(authorize) {
        return authorize(this.name);
      }]
    }
  })

   .state('app.sair', {
    url: '/sair',
    views: {
      'menuContent': {
        templateUrl: 'templates/sair.html'
      }
    }
  });

   $urlRouterProvider.otherwise('/login');
 });