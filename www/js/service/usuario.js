app.service('Usuario', ['DB', function(DB) {
    this.get = function(params) {

        params = params || {};

        var response = [];
        var where = '';

        if (params.id) {
            where += " AND usu.ID = '" + params.id + "' ";
        }

        var sql = "SELECT usu.* FROM USUARIO usu  WHERE 1=1 " + where + " ";

        return DB.query(sql)
        .then(function(result) {
            return DB.fetch(result);
        });
    };

    this.validar = function(params) {
      
        params = params || {};

        var sql = "SELECT usu.* FROM USUARIO usu WHERE EMAIL_APP = '" + params.email + "' AND SENHA_APP = '" + params.senha + "'  ";
                 
        return DB.query(sql)
        .then(function(result) {
            return DB.fetch(result);
        });
    };

    return  {
        get: this.get,
        validar: this.validar,
    };
}]);
