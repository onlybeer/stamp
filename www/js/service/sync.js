app.factory('Sync', function($q, $http, DB) {
    var _sync = {};
    _sync.table = "";
    _sync.all = function(router, table, campos, json) {
        var deferred = $q.defer();
        var v = 0;

        var versao = localStorage.getItem(table) || 0;
        

        function sucesso(results, status, headers, config) {
            if (!results.length) {
                console.log('sem registro novos em: ' + router);
                deferred.resolve(status);
            }
            
            function successHandler() {
      
                v++;
                if (v == results.length) {
                    deferred.resolve(status);
                }
            }

            var totalResults = results.length;
            var key = 0;
            var insert = "";
            var s = 0;

            DB.db.transaction(function(tx) {
       
                for (var i = 0; i < totalResults; i++) {
                    var item = results[i];
                    var guest = [];
                    var values = [];
 
                    if (item.DELETADO == 1 || item.ATIVO == '0') {
                        tx.executeSql("DELETE FROM " + table + " WHERE ID = ?", [item.ID], successHandler, DB.errorHandler);
                    } else {
                        //console.log("entrou no for");
                        for (var c = 0; c < campos.length; c++) {
                            guest[c] = '?';
                            // values[c] = item[campos[c]] != null ? item[campos[c]] : '';
                            values[c] = item[campos[c]] != null ? '"'+item[campos[c]]+'"' : "''";
                        }
                        // tx.executeSql("REPLACE INTO " + table + " (" + campos.toString() + ") VALUES (" + guest.toString() + ")", values, successHandler, DB.errorHandler);
                        if(s == 0){
                            insert += "REPLACE INTO " + table + " (" + campos.toString() + ") VALUES  ";
                        }

                        insert += " (" + values.toString() + ")";
                        s++;
                        if(s == 499 || i == totalResults-1){
                            insert += ";";
                            var query = insert;
                            insert = "";
                            s = 0;
                            tx.executeSql(query, [], successHandler, DB.errorHandler);
                        } else {
                            insert += ",";
                            successHandler();
                        }
                    }
                    if(i == totalResults-1){
                        localStorage.setItem(table, item.DATA_UPD);
                        tx.executeSql("REPLACE INTO VERSAO (TABELA, DATA_UPD) VALUES (?,?)", [table, item.DATA_UPD]);
                    }
                }
                
            });
        }
        if(!json){
            $http.get(api + router + '/versao/' + versao)
            .success(sucesso).
            error(function(data, status, headers, config) {
                deferred.reject(status);
            });
        } else {
            sucesso(json, 200);
        }

        return deferred.promise;
    };

    _sync.usuario = function(json) {

        var campos = [
            'ID',
            'ID_ERP',
            'RAZAO_SOCIAL',
            'NOME_FANTASIA',
            'EMAIL',
            'EMAIL_APP',
            'SENHA',
            'SENHA_APP',
            'UF',
            'REGIAO',
            'DATA_UPD',
        ];
        return _sync.all('usuario', 'USUARIO',campos,json);
    };


    return _sync;
});
