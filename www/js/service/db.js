app.factory('DB', function($q, $rootScope, $cordovaSQLite) {
    var self = this;
    self.db = null;
    self.version = 10;
    self.tabelas = [
    'USUARIO',
    'VERSAO'
    ];

    self.errorHandler = function(transaction, error) {
        // alert(error.message + ' (Code ' + error.code + ')');
        console.log(error.message);
        return false;
    };

    self.dataHandler = function(transaction, results) {
        // var string = "Nenhum erro ao executar o sql";
    };

    self.clearDatabase = function() {
        self.db.transaction(function(tx) {
            for (var i = 0; i < self.tabelas.length; i++) {
                var sql = 'DROP TABLE ' + self.tabelas[i];
                tx.executeSql(sql, [], self.dataHandler, self.errorHandler);
                localStorage.setItem(self.tabelas[i], 0);
            }
        });
    };

    self.init = function() {
        if(self.db !== null){
            return false;
        }
        if (window.cordova && window.sqlitePlugin) {
            // alert('sqlite');
            console.log('sqlite');
            self.db = window.sqlitePlugin.openDatabase('banco.db', '', 'meebe', -1);
        } else {
            // alert('websql');
            console.log('websql');
            self.db = window.openDatabase('banco.db', '', 'meebe', 50 * 1024 * 1024);
        }

        // angular.forEach(DB_CONFIG.tables, function(table) {
        //     var columns = [];
        //
        //     angular.forEach(table.columns, function(column) {
        //         columns.push(column.name + ' ' + column.type);
        //     });
        //
        //     var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
        //     self.query(query);
        //     console.log('Table ' + table.name + ' initialized');
        // });

        if (self.db.version != self.version) {
            if (!window.sqlitePlugin) {
                self.db.changeVersion(self.db.version, self.version);
            }
            self.clearDatabase();
        }

        // USUARIO
        self.query('CREATE TABLE IF NOT EXISTS "USUARIO" ("ID" TEXT PRIMARY KEY, "ID_APP" TEXT, "ID_ERP" TEXT, "ID_TAB_PRECO" TEXT, "ID_USUARIO" TEXT, "RAZAO_SOCIAL" TEXT, "NOME_FANTASIA" TEXT, "CGC" TEXT, "IE" TEXT, "ATIVO" INTEGER NOT NULL DEFAULT 1, "TOKEN" TEXT, "SENHA" TEXT, "EMAIL" TEXT, "SENHA_APP" TEXT, "EMAIL_APP" TEXT, "UF" TEXT, "REGIAO" TEXT, "DATA_ADD" DATE, "DATA_UPD" DATE)', [], self.dataHandler, self.errorHandler);
        // VERSAO
        self.query('CREATE TABLE IF NOT EXISTS "VERSAO" ("TABELA" TEXT PRIMARY KEY, "DATA_UPD" TEXT)', [], self.dataHandler, self.errorHandler);
    };

    self.prepare = function(table, data){
        var campos = [];
        var values = [];
        var guess = [];
        var sql = "";
        for(var name in data) {
            campos.push(name);
            var value = data[name];
            values.push("'"+value+"'");
            guess.push('?');
        }
        if(data.ID){
            sql = "UPDATE "+table+" SET ("+campos.toString()+") = ("+values.toString()+") WHERE ID = '"+data.ID+"' ";
        } else {
            sql = "INSERT INTO "+table+" SET ("+campos.toString()+") VALUES ("+values.toString()+") ";
        }
        console.log(sql);
    };

    self.query = function(query, bindings) {

        $rootScope.loading = true;

        var deferred = $q.defer();
        bindings = typeof bindings !== 'undefined' ? bindings : [];

        self.db.transaction(function(transaction) {
            transaction.executeSql(query, bindings, function(transaction, result) {
                deferred.resolve(result);
                $rootScope.loading = false;
            }, function(transaction, error) {
                console.log(error);
                deferred.reject(error);
                $rootScope.loading = false;
            });
        });

        return deferred.promise;
    };

    self.fetchAll = function(result) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }

        $rootScope.loading = false;
        return output;
    };

    self.fetch = function(result) {
        var output = false;

        if(result.rows.length){
            output = result.rows.item(0);
        }

        $rootScope.loading = false;
        return output;
    };

    self.create =  [
    {
        name: 'USUARIO',
        columns: [
        {name: 'ID', type: 'text primary key'},
        {name: 'ID_APP', type: 'text'},
        {name: 'ID_ERP', type: 'text'},
        {name: 'ID_TAB_PRECO', type: 'integer'},
        {name: 'ID_USUARIO', type: 'text'},
        {name: 'RAZAO_SOCIAL', type: 'text'},
        {name: 'NOME_FANTASIA', type: 'text'},
        {name: 'CGC', type: 'text'},
        {name: 'IE', type: 'text'},
        {name: 'ATIVO', type: 'integer not null default 1'},
        {name: 'TOKEN', type: 'text'},
        {name: 'SENHA', type: 'text'},
        {name: 'SENHA_APP', type: 'text'},
        {name: 'EMAIL', type: 'text'},
        {name: 'EMAIL_APP', type: 'text'},
        {name: 'UF', type: 'text'},
        {name: 'REGIAO', type: 'text'},
        {name: 'DATA_ADD', type: 'date'},
        {name: 'DATA_UPD', type: 'date'},
        ]
    },{
        name: 'VERSAO',
        columns: [
        {name: 'TABELA', type: 'text'},
        {name: 'DATA_UPD', type: 'text'},
        ]
    }
    ];

    return self;
});
