app.service('CLIENTES', function() {
	function get(){
		var Clientes = [
		{
			'ID':1,
			'RAZAO_SOCIAL':'Empresa Teste 1',
			'NOME_FANTASIA':'Teste1',
			'UF':'RS',
			'CIDADE':'Porto Alegre',
			'CGC':'999999999999999',
			'ID_ERP':'000001',
			'IE':'ISENTO',
			'TELEFONE':'9999-9999',
			'ENDERECO':'RUA ANITA GARIBALDI',
			'CEP':'90450000',
			'NOME':'RESPONSAVEL 1',
			'EMAIL':'empresa@teste.com.br',

		},
		{
			'ID':2,
			'RAZAO_SOCIAL':'Empresa Teste 2',
			'NOME_FANTASIA':'Teste2',
			'UF':'AM',
			'CIDADE':'MANAUS',
			'CGC':'888888888888888',
			'ID_ERP':'000001',
			'IE':'ISENTO',
			'TELEFONE':'9999-9999',
			'ENDERECO':'RUA TESTE',
			'CEP':'90450000',
			'NOME':'RESPONSAVEL 1',
			'EMAIL':'empresa@teste.com.br',


		},
		{
			'ID':3,
			'RAZAO_SOCIAL':'Empresa Teste 3',
			'NOME_FANTASIA':'Teste3',
			'UF':'PR',
			'CIDADE':'PARANA',
			'CGC':'777777777777777',
			'ID_ERP':'000001',
			'IE':'ISENTO',
			'TELEFONE':'9999-9999',
			'ENDERECO':'RUA TESTE',
			'CEP':'90450000',
			'NOME':'RESPONSAVEL 1',
			'EMAIL':'empresa@teste.com.br',

		},
		{
			'ID':4,
			'RAZAO_SOCIAL':'Empresa Teste 4',
			'NOME_FANTASIA':'Teste4',
			'UF':'BA',
			'CIDADE':'SALVADOR',
			'CGC':'666666666666666',
			'ID_ERP':'000001',
			'IE':'ISENTO',
			'TELEFONE':'9999-9999',
			'ENDERECO':'RUA TESTE',
			'CEP':'90450000',
			'NOME':'RESPONSAVEL 1',
			'EMAIL':'empresa@teste.com.br',

		},
		{
			'ID':5,
			'RAZAO_SOCIAL':'Empresa Teste 5',
			'NOME_FANTASIA':'Teste5',
			'UF':'SP',
			'CIDADE':'SAO PAULO',
			'CGC':'555555555555555',
			'ID_ERP':'000001',
			'IE':'ISENTO',
			'TELEFONE':'9999-9999',
			'ENDERECO':'RUA TESTE',
			'CEP':'90450000',
			'NOME':'RESPONSAVEL 1',
			'EMAIL':'empresa@teste.com.br',

		},
		{
			'ID':6,
			'RAZAO_SOCIAL':'Empresa Teste 6',
			'NOME_FANTASIA':'Teste6',
			'UF':'SC',
			'CIDADE':'FLORIANOPOLIS',
			'CGC':'444444444444444',
			'ID_ERP':'000001',
			'IE':'ISENTO',
			'TELEFONE':'9999-9999',
			'ENDERECO':'RUA TESTE',
			'CEP':'90450000',
			'NOME':'RESPONSAVEL 1',
			'EMAIL':'empresa@teste.com.br',

		}
		];

		return Clientes;
	}

	return {
		get: get,
	};

});