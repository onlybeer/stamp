app.controller('LoginController', function($rootScope, $scope, $state, $ionicPopup, Usuario, Sync, DB, $http, $ionicPlatform,$timeout) {

    $rootScope.deviceId = localStorage.getItem('deviceId');

    if(!$rootScope.deviceId){
        $rootScope.deviceId = 'FZY2SL';
        localStorage.setItem('deviceId', $rootScope.deviceId);
    }

    $scope.login = {};
    $scope.login.EMAIL = localStorage.getItem('email');
    $scope.login.SENHA = localStorage.getItem('senha');

    $scope.logar = function(){

        $scope.error = '';

        if($scope.login.EMAIL && $scope.login.SENHA){

            Usuario.validar({
                email: $scope.login.EMAIL,
                senha: $scope.login.SENHA,
            }).then(function(res){
                if(res){
                    if(localStorage.getItem('email') != $scope.login.EMAIL){
                        // Pedido.clearPedidoTemp();
                    }
                    localStorage.setItem('email', $scope.login.EMAIL);
                    localStorage.setItem('senha', $scope.login.SENHA);
                    localStorage.setItem('usuarioId', res.ID);
                    $rootScope.usuario = {
                        ID: res.ID,
                        NOME: res.NOME_FANTASIA,
                        EMAIL: res.EMAIL,
                        UF: res.UF,
                        REGIAO: res.REGIAO,
                    };
                    $state.go('app.clientes');
                } else {
                    $scope.error = 'Dados inválidos';
                }
            });
        }
    };
    $scope.openDialogSincronizar = function() {
        $scope.popSync = $ionicPopup.show({
            templateUrl: 'templates/dialogs/dialog-sincronizar.html',
            title: '<strong>MEEBE</strong>',
            cssClass: 'sincronizar',
            subTitle: '<strong>ID: ' + $rootScope.deviceId + '</strong><br/>' + $rootScope.updateOn + '',
            scope: $scope
        });
    };
   
});
