app.controller('CatalogoCtrl', function($scope, $ionicModal,$ionicPopover, $timeout,PRODUTO,$timeout,$filter) {

	
	$scope.Produtos = PRODUTO.get();
	$scope.searchTerm = "";


	$scope.loadMore = function() {

		if ($scope.Produtos.length) {
			$scope.noMoreItemsAvailable = false;
			$scope.Produtos.concat($scope.Produtos);
		} else {
			$scope.noMoreItemsAvailable = true;
		}

		$scope.$broadcast('scroll.infiniteScrollComplete');
	};
	$scope.tab = 'lista';
	
	$scope.changeTab = function(tab) {

		$scope.tab = tab;

	};

})