app.controller('SincronizarController', function($rootScope, $scope, Sync, $ionicPopup, DB, $window, $filter,$http) {
	$scope.params = $scope.params || {};
	$scope.stop = 0;

	var i = 0;
	var apis = [
	{ TITULO : 'Usuários', API: Sync.usuario },
	];

	$scope.perc = 0;
	$scope.sincronizar = function() {

		if($scope.stop){
			return false;
		}
		var api = apis[i];
		$scope.msg = 'Atualizando ' + api.TITULO;
		$http.get("zip/usuario.json").success(function(json){
		api.API(json).then(function() {
			i++;
			if (i < apis.length) {
				$scope.sincronizar();
			} else {
				var dataHora = $filter('date')(Date.now(), "dd/MM/yyyy 'às' h:mma");
				$rootScope.updateOn = dataHora;
				localStorage.setItem('updateOn', dataHora);
                // $window.location.reload();
                $scope.popSync.close();
                if($scope.params.callback){
                	$scope.params.callback();
                }
            }
            $scope.perc = i * (100/apis.length);
        });
	});
	};

	$scope.stopSync = function(){
		$scope.stop = 1;
	};

	
	$scope.sincronizar();








});
