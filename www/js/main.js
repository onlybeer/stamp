app.controller('AppController', function($rootScope, $scope, $state, $ionicModal, $timeout, $ionicActionSheet, $ionicPopup,$filter, $ionicHistory, $q) {

	$scope.sair = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Sair',
            template: '<p class="text-center m-b-none">Sair da sessão de <br/><strong>' + $rootScope.usuario.NOME + '<strong>?</p>',
            buttons: [{
                text: 'Cancelar'
            }, {
                text: 'Sim',
                type: 'button-assertive',
                onTap: function(e) {
                    localStorage.setItem('usuarioId', '');
                    localStorage.setItem('senha', '');
                    $rootScope.usuario = null;
                    $timeout(function(){
                        $rootScope.redirect('login');
                    });
                }
            }]
        });
    };


	$scope.openDialogSincronizar = function() {

        $scope.params = {
            callback: function(){
                $rootScope.redirect('app.clientes');
                $rootScope.showToaster('Aplicativo <b>atualizado</b> com sucesso!');
            }
        };
        $scope.popSync = $ionicPopup.show({
            templateUrl: 'templates/dialogs/dialog-sincronizar.html',
            title: '<strong>MEEBE</strong>',
            cssClass: 'sincronizar',
            subTitle: '<strong>ID: ' + $rootScope.deviceId + '</strong><br/>' + $rootScope.updateOn + '',
            scope: $scope,
        });
    };



	$scope.openDialogConfiguracoes = function() {

		var confirmPopup = $ionicPopup.confirm({
			title: 'Configurações - Versao Demo',
			template: '<p class="text-center m-b-none">Função Desabilitada</p>',
			buttons: [{
				text: 'Ok'

			}]
		});
	};

	$rootScope.redirect = function(state) {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go(state);
        $timeout(function() {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        });
    };

});
