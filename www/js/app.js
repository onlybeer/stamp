if (!localStorage.getItem('deviceId')) localStorage.setItem('deviceId', '');
if (!localStorage.getItem('updateOn')) localStorage.setItem('updateOn', '');
if (!localStorage.getItem('email')) localStorage.setItem('email', '');
if (!localStorage.getItem('senha')) localStorage.setItem('senha', '');

var app = angular.module('starter', ['ionic','QuickList','ngCordova'])

.config(['$ionicConfigProvider', function($ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(2);
    $ionicConfigProvider.templates.maxPrefetch(0);
    $ionicConfigProvider.views.forwardCache(false);
}])

.run(function($rootScope, $ionicPlatform, DB, $state, $window, $ionicPopup, $cordovaStatusbar) {
    $rootScope.deviceId = localStorage.getItem('deviceId');
    $rootScope.updateOn = localStorage.getItem('updateOn');


    $rootScope.$on('$stateChangeSuccess', function(event, next) {
        if(!localStorage.getItem('usuarioId')){
            // $state.go('login');
        }
    });
	$ionicPlatform.ready(function() {
		DB.init();
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
        $cordovaStatusbar.style(3);
    });
})


